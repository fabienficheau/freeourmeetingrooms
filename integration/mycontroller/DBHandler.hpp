#pragma once

extern "C"
{
#include "hiredis.h"
}

#include "MyMessage.h"

namespace freerooms
{

class DBHandler
{
public:
    DBHandler();
    ~DBHandler();
    void init();
    void pushValue(MyMessage&);

    redisContext* _redisCtx;
private:
};

}
