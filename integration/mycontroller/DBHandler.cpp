#include "stdint.h"
#include "DBHandler.hpp"
#include <iostream>
#include <sstream>

namespace freerooms
{

DBHandler::DBHandler():
        _redisCtx(0)
{
    init();
}

DBHandler::~DBHandler()
{
    if(_redisCtx)
    {
        std::cout << "Freeing redis ctx..";
        redisFree(_redisCtx);
        _redisCtx = 0;
    }
    else
    {
        std::cout << "Not freeing redis ctx..";
    }
}

void DBHandler::pushValue(MyMessage& iMsg)
{
    //timestamp
    time_t t = time(0);   // get time now
    size_t aTs = (size_t)t;


//    std::stringstream aScore;
//    aScore << aTs;
//    std::stringstream aValue;
//    aValue << aTs << ":" << iMsg.getString();
//
//    printf("titi: %s\n",iMsg.getString());
//    printf("titititi %d;%d;%d;%d;%d\n",iMsg.sender, iMsg.sensor, mGetCommand(iMsg), mGetAck(iMsg), iMsg.type);
//    std::stringstream aSensor;
//    aSensor << (size_t)iMsg.sender << "_" << (size_t)iMsg.sensor;

    char buf[50];
    memset(buf,0,sizeof(buf));
    sprintf(buf,"%s%d_%d %u %u:%s","ZADD sensors:sensor_",iMsg.sender,iMsg.sensor,aTs,aTs,iMsg.getString());
//    sprintf(buf,"%s%d_%d %u %s","ZADD sensors:sensor_",iMsg.sender,iMsg.sensor,aTs,iMsg.getString());

//    std::stringstream aCommand;
//    aCommand << "ZADD sensors:sensor_" <<
//            (size_t)iMsg.sender << "_" << (size_t)iMsg.sensor <<
//            " " <<
//            aTs << //redis score
//            " " <<
//            aTs << ":" << iMsg.getString() //redis value
//            ;
//
////            aSensor.str() << " " << aScore.str() << " " << aValue.str();
//
    std::cout << "redis cmd: " << buf << std::endl;

//
    redisReply* aReply=0;

    aReply = (redisReply*)redisCommand(_redisCtx,buf);
    if(!aReply)
    {
        printf( "Error\n");
    }
    else
    {
        if ( aReply->type == REDIS_REPLY_ERROR )
        {
                printf( "Error: %s\n", aReply->str );
        }
        freeReplyObject(aReply);
    }
//    sleep(1);
}

void DBHandler::init()
{
    std::cout << "DBHandler initializing.." << std::endl;

    redisReply *reply;
//    const char *hostname = "127.0.0.1";
//    const char *hostname = "192.168.1.44";
//    const char *hostname = "NCEVCFTM201704";
    const char *hostname = "10.64.167.10";
    int port = 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    _redisCtx = redisConnectWithTimeout(hostname, port, timeout);
    if (_redisCtx == 0 || _redisCtx->err)
    {
       if (_redisCtx)
       {
           printf("Connection error: %s\n", _redisCtx->errstr);
           redisFree(_redisCtx);
           _redisCtx = 0;
       }
       else
       {
           printf("Connection error: can't allocate redis context\n");
       }
    }
    /* PING server */
    reply = (redisReply*)redisCommand(_redisCtx,"PING");
    printf("PING: %s\n", reply->str);
    freeReplyObject(reply);

    std::cout << "DBHandler init OK" << std::endl;
}

}
