#pragma once
//#include <RF24/RF24.h>
#include "MyGateway.h"

namespace freerooms
{

//extern static MyMessage _receivedMsg;
//extern static bool _msgReceived;
//extern MyMessage kReceivedMsg;
//extern bool kMsgReceived;


class RFHandler
{
public:
    RFHandler(DBHandler&);
    ~RFHandler();
    void init();
    void processRadioMessage();

    static MyMessage _receivedMsg;
    static bool _wasMsgReceived;

private:
    MyGateway* _gateway;
    DBHandler& _dbHandler;
//    MyMessage* _receivedMsg;
//    uint8_t* _wasMsgReceived;
};

}
