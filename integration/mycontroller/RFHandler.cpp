#define private public

#include "RFHandler.hpp"
#include "DBHandler.hpp"
#include <iostream>
#include "MyGateway.h"

namespace freerooms
{

//MyMessage RFHandler::_receivedMsg;
//bool RFHandler::_wasMsgReceived = false;
//MyMessage kReceivedMsg;
//bool kMsgReceived = false;

RFHandler::RFHandler(DBHandler& iDbHandler):
        _dbHandler(iDbHandler)
{
    init();
}

void msgCallback(MyMessage& msg,DBHandler& idbHandler)
{
    printf("received!\n");
//    RFHandler::_wasMsgReceived = false;
    //            MyMessage& msg = ;
    idbHandler.pushValue(msg);
    //    RFHandler::_receivedMsg = msg;
//    RFHandler::_wasMsgReceived= true;
//    printf("[CALLBACK]");
//     printf("[CALLBACK]%s", msg.getString());
}

RFHandler::~RFHandler()
{
    delete _gateway;
}

void RFHandler::processRadioMessage()
{
    _gateway->processRadioMessage();
//    MyMessage& msg = _gateway->getLastMessage();
}

void RFHandler::init()
{
    std::cout << "RFHandler initializing.." << std::endl;
    _gateway = new MyGateway(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ,1);
//    _gateway = new MyGateway(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ, 30000);

//    _msgReceived = new MyMessage();
//    _wasMsgReceived = new uint8_t(0);

    printf("totooff:%x", ((char*)&(_gateway->inclusionTime) -(char*) _gateway));
    printf("toto2:%u",sizeof(_gateway));
    printf("toto3:%u",sizeof(MyGateway));
    printf("toto4:%u",MAX_PAYLOAD);


    if (_gateway == NULL)
    {
        throw std::string("_gateway is null!");
    }
    _gateway->begin(RF24_PA_LEVEL_GW, RF24_CHANNEL, RF24_DATARATE, NULL);
//    _gateway->_receivedMsg = _msgReceived;
//    _gateway->_wasMsgReceived = _wasMsgReceived;
    _gateway->setMessageHandler(msgCallback);
    _gateway->_dbHandler = &(this->_dbHandler);

    std::cout << "RFHandler init OK" << std::endl;
}

}
