#inspired from https://github.com/thehawkes/pymygw

MySensorDescriptor = {}

MySensorMessageType = {}
MySensorMessageType['0'] = {'name':'presentation', 'description':'Sent by a node when they present attached sensors. This is usually done in setup() at startup.'}
MySensorMessageType['1'] = {'name':'set', 'description':'This message is sent from or to a sensor when a sensor value should be updated'}
MySensorMessageType['2'] = {'name':'request', 'description':'Requests a variable value (usually from an actuator destined for controller).'}
MySensorMessageType['3'] = {'name':'internal', 'description':'This is a special internal message.'}
MySensorMessageType['4'] = {'name':'stream', 'description':'Used for OTA firmware updates'} 

MySensorDescriptor['messagetype'] = MySensorMessageType