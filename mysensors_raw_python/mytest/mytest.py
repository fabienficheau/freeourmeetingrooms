import serial
import logging
import logging.handlers
import config
import protocol
import redis
import time

log = logging.getLogger('fftest')
log.propagate = False
if not log.handlers:
    formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
    handler = logging.handlers.RotatingFileHandler(config.LogFile, maxBytes=4000000, backupCount=5)
    handler.setFormatter(formatter)
    log.addHandler(handler)
if config.DEBUG:
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)

def describeMessage(message):
    copiedMsg = message.copy()
    for key,value in copiedMsg.iteritems():
        if key=="messagetype":
            messagetype = message['messagetype']
            copiedMsg['messagetype']=protocol.MySensorDescriptor['messagetype'][messagetype]['name']
    return str(copiedMsg)

def createMessage(line):
    message = {}
    splittedLine = line.split(";")
    nbValues = 6
    if(len(splittedLine)!=nbValues):
        print "error: " + str(nbValues) + " fields expected in serial output, found " +\
            str(len(splittedLine)) + " instead. Raw: " + str(splittedLine)
    message['nodeid']=splittedLine[0]
    message['childid']=splittedLine[1]
    message['messagetype']=splittedLine[2]
    message['subtype']=splittedLine[3]
    message['payload']=splittedLine[4]
    message['value']=splittedLine[5].rstrip()
    return message

def initRedis():
    r_server = redis.Redis('10.64.167.10') 
    return r_server 

def addToDb(r_server,nodeId,sensorId,value):
    key = "sensors:sensor_" + nodeId + "_" + sensorId
    timestamp = str(int(time.time()))
    score = timestamp
    member = timestamp + ":" + str(value)
    print "key: " + key
    print "score: " + score
    print "member: " + member 
    r_server.zadd(key,member,score)

def pushToDb(r_server,pubsub,nodeId,sensorId,value):
    if(nodeId=='3' and sensorId=='1'):
        print "publish" + str(value)
        r_server.publish("sensor_3_1",value)
    if(nodeId=='3' and sensorId=='2'):
        print "publish" + str(value)
        r_server.publish("sensor_3_2",value)
    if(nodeId=='2' and sensorId=='1'):
        print "publish" + str(value)
        r_server.publish("sensor_2_1",value)
    if(nodeId=='4' and sensorId=='1'): #temp
        print "publish" + str(value)
        r_server.publish("sensor_4_1",value)
    if(nodeId=='4' and sensorId=='0'): #humidity
        print "publish" + str(value)
        r_server.publish("sensor_4_0",value)
    if(nodeId=='7' and sensorId=='1'): #humidity
        if(value=='1'):
            value='0'
        else:
            value='1'
        print "publish" + str(value)
        r_server.publish("sensor_7_1",value)
        

def processMessage(msg,r_server,pubsub):
    print describeMessage(msg)
    addToDb(r_server,msg['nodeid'],msg['childid'],msg['value'])
    pushToDb(r_server,pubsub,msg['nodeid'],msg['childid'],msg['value'])
    

def readSerial(r_server,pubsub):
    ser = serial.Serial('/dev/ttyMySensorsGateway')
    ser.flushInput()
    ser.flushOutput()
    
    print ser.name
    while True:
        line = ser.readline()
        if line != "":
            print line
            structuredMsg = createMessage(line)
            processMessage(structuredMsg,r_server,pubsub)

if __name__ == '__main__':
    r_server = initRedis()
    pubsub = r_server.pubsub()
    readSerial(r_server,pubsub)